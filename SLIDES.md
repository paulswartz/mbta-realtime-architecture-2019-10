---
title: MBTA Realtime Architecture 2019-10
revealOptions:
    defaultTiming: 20
---

<!-- .slide: data-timing="5" -->
# MBTA Realtime Architecture

oct 2019
[bit.ly/mbta-realtime-2019-10](http://bit.ly/mbta-realtime-2019-10)

Note: thanks everyone for coming! I'm Paul Swartz, lead architect for MBTA's
Customer Technology Department. Today I'll be giving a quick overview of the
architecture of our realtime data systems.

---

<!-- .slide: data-timing="5" -->

<img src="assets/diagram.png" />

Note: this is a simplified diagram we use for internal presentations (there's
an even more detailed version) but with only 5 minutes I'm just going to do
the biggest hits today.

---

> "small pieces, loosely joined"

David Weinberger

Note: This was a book written about the web in 2002, but also applies nicely
to our architecture @ MBTA. we take advantage of being able to build on small
applicarions on Amazon Web Services, and try to have each application
responsible for ~one thing.

---

<!-- .slide: data-timing="5" -->
# small pieces

Note: first, let's talk about the small pieces
----

## GTFS

HASTUS

spreadsheets

custom processing pipeline

Note: I don't need to tell you all about GTFS. Our bus/subway schedules are
built in HASTUS, and we get other schedules as a set of spreadsheets. We put
out new files ~weekly to incorporate shuttles and other disruptions, but
looking to improve on both the frequency and the latency. Custom processing
currently built on Python/Pandas, used to incorporate non-GTFS input files as
well as modifications for shuttles and other disruption.

----

## Commuter Rail

vehicle locations

boarding status

Note: We have two applications for Commuter Rail: one reporting vehicle locations,
the other reporting boarding statuses like "All Aboard on track 3".

----

## Bus

vehicle locations

Note: we have a few different sources of bus location information
(TransitMaster, Samsara, other feeds for contracted routes and vehicles), and
they're combined together by one application as sometimes we have multiple
sources for the same vehicles and we want to provide a consistent view of
each vehicle's current status.

----

## Subway

vehicle locations + predictions

Note: this application receives messages from our Occupancy Control System
about vehicle locations, and uses that along with local knowledge about our
tracks to generate predictions.

----

## Ferry

`¯\_(ツ)_/¯`

Note: sorry boat riders: we don't have any realtime information on boat
locations. Other than the occasional cancelation (handled by the alerts
system), they're very reliable in terms of schedule adherence.

----

## Swiftly

bus predictions

commuter rail predictions

Note: we had a bake-off with several different vendors for bus predictions,
which Swiftly won. they're providing predictions for bus and commuter rail
based on those vehicle positions feeds I mentioned earlier.

----

## Alerts

IBI

Note: we use the Alerts UI software managed by IBI. Our Public Information
folks enter the alerts, and we consume the output in other applications.

---

<!-- .slide: data-timing="5" -->
# loosely joined

Note: how do we combine all these pieces together?

----

# Concentrate

the hub

Note: the hub of all the activity: the big circle in the diagram from
earlier. It builds our realtime files by combining all the incoming data from
different feeds. Also does some processing to handle alerts, mostly by stops
that aren't being served as SKIPPED, or trips than aren't running as
CANCELED.

----

## GTFS-Realtime (Enhanced)

standard(-ish)

Note: we've standardized on GTFS-Realtime Enhanced, a JSON format based on
GTFS-Realtime. Effectively, it's GTFS-Realtime expanded into JSON with
additional non-standard keys. Using JSON is ~20% less efficient after
compression, but it's much easier to work.

We still provide standard GTFS-Realtime files: Concentrate produces both the
standard and Enhanced outputs.

----

## S3 / HTTPS

Note: Generally, we have applications upload their GTFS-Realtime files to S3
buckets, as frequently as they can manage. From there, Concentrate and other
applications can poll to get updates, without needing to connect directly to
the application.

For external vendors, we ask them to provide an HTTP GET endpoint in a
standard format, and we treat it like any of our internal feeds.

This decouples the apps from each other, so they don't need to directly
connect. This allows us to avoid tricky firewall issues. It also degrades
gently, where the information goes state rather than disappearing
entirely. We have monitoring to detect stale data before riders report it to
us.

---

## Outputs

public API / website

Google/Apple/Transit

internal tools

Note: we base applications on the public data wherever possible. For some
applications (which need operator information, for example), we try to use
standard formats even if the feeds themselves are not public. This allows us
to rely on standardized parsers, rather than each application needing to
consume or produce a different format. It also forces us to think about how
to integrate the data across different applications.

We also use the public data internally. For example: on-time-performance
tracking, and our new Skate bus dispatching software.  All that is based on
the GTFS-Realtime Enhanced output data.

---

## Future

streaming

historic data

Note: streaming data through these pipelines would allow us to cut down on
some of the latency. We're at ~15 seconds right now from a vehicle reporting
a location to that location appearing in Google Maps or Transit, but we'd
always prefer that to be faster.

We currently have historic data in a few different places throughout our
department and the rest of the MBTA, but it's not organized, making it hard to
know what we have and to work with it. We'd like to improve that organization
so all teams can get what they need from all this realtime data.

---

## [github/mbta](https://github.com/mbta)

api / concentrate / dotcom

prediction_analyzer / transit-performance

Note: we open-source as much as we can, and contribute back to the
applications and libraries we're building on (Elixir / Python /
TypeScript). you can check it out at our GitHub page.

---

# Thanks!

* @paulswartz
* pswartz@mbta.com
* [mbta.com/developers](https://www.mbta.com/developers)
